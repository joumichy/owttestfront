import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {StorageMap} from "@ngx-pwa/local-storage";
import {from, map, Observable} from "rxjs";
import {BoatModel} from "../../models/Boat";
import {headers, headersWithToken} from "../../util/util";
import {UserService} from "../user/user.service";
import {BoatResponse} from "../../models/BoatResponse";

@Injectable({
  providedIn: 'root'
})
export class BoatService {

  private url: string = environment.apiUrl;
  private proxy: string = environment.proxy;

  constructor(private _httpClient: HttpClient, private userService: UserService) {
  }

  async getAllBoats(): Promise<Observable<BoatModel[]>> {
    const path: string = "/boat/all";
    const finalpath: string = this.proxy + this.url + path;
    const token = await this.userService.getUserToken();
    return this._httpClient.get<BoatModel[]>(finalpath, {headers: headersWithToken(token)});
  }

  async createBoat(name: string, size: number, description: string) : Promise<Observable<BoatResponse>> {
    const path: string = "/boat/create";
    const finalpath: string = this.proxy + this.url + path;
    const token = await this.userService.getUserToken();

    const data = {
      name,
      size,
      description
    }
    return this._httpClient.post<BoatResponse>(finalpath, data, {headers: headersWithToken(token)});

  }

  async deleteBoat(boatId: number) {

    const path: string = "/boat/delete";
    const finalpath: string = this.proxy + this.url + path;
    const token = await this.userService.getUserToken();
    const params = {
      boatId
    }
    return this._httpClient.delete<BoatResponse>(finalpath, {headers: headersWithToken(token), params},);

  }

  async editBoat(id: number | undefined, name: string, size: number, description: string): Promise<Observable<BoatResponse>> {
    const path: string = "/boat/edit";
    const finalpath: string = this.proxy + this.url + path;
    const token = await this.userService.getUserToken()
    const data = {
      id,
      name,
      size,
      description
    }
    return this._httpClient.put<BoatResponse>(finalpath, data, {headers: headersWithToken(token)},);
  }
}
