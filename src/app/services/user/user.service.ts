import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {StorageMap} from "@ngx-pwa/local-storage";
import {Observable} from "rxjs";
import {UserModel} from "../../models/UserModel";
import {environment} from "../../../environments/environment";
import { headers} from "../../util/util";
import {BoatResponse} from "../../models/BoatResponse";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url : string = environment.apiUrl;
  private proxy : string = environment.proxy;

  constructor(private _httpClient : HttpClient,
              private storage : StorageMap) {
  }

  loginUser(username : string, password : string) : Observable<UserModel>{
    const path : string = "/user/signin";
    const finalPath = this.proxy+this.url+path;

    const data = {
      username,
      password
    }
    return this._httpClient.post<UserModel>(finalPath, data, {headers} )
  }

  signUpUser(username : string, password : string) : Observable<string>{
    const path : string = "/user/signup";
    const finalPath = this.proxy+this.url+path;
    const data = {
      username,
      password
    }
    return this._httpClient.post<string>(finalPath, data, {headers, responseType : "text" as "json"} )
  }


  setUserData(user : UserModel){
    this.storage.set('user', user).subscribe(value => {});
    this.storage.set('token',user.token).subscribe(value => {})
  }

   getUserToken() : Promise<unknown> {
    return this.storage.get('token').toPromise();
  }

  getUserData() : Promise<unknown> {
    return this.storage.get('user').toPromise();
  }
  clearData(){
    this.storage.clear();
  }
}
