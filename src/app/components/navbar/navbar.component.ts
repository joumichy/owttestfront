import {Component, Input, OnInit} from '@angular/core';
import {UserModel} from "../../models/UserModel";
import {UserService} from "../../services/user/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  public currentUser : UserModel | undefined;

  constructor(private  userService : UserService, private router : Router) { }

  async ngOnInit(): Promise<void> {
    this.currentUser = await this.userService.getUserData() as UserModel
  }

  async disconnect() {
    await this.userService.clearData();
    await this.router.navigate(['/login']);
  }
}
