import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user/user.service";
import {Router} from "@angular/router";
import {SnackbarService} from "../../services/snackbar/snackbar.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username : new FormControl('', [Validators.required],),
    password : new FormControl('', [Validators.required],)

  })
  constructor(private  userService : UserService,
              private router : Router,
              private snackBar : SnackbarService) { }

  ngOnInit(): void {
  }


  get username(){
    return this.loginForm.get('username')
  }
  get password(){
    return this.loginForm.get('password')
  }

  login(){
    if(this.loginForm.valid){
      this.userService.loginUser(this.username?.value, this.password?.value).subscribe(async value => {
        await this.userService.setUserData(value);
        await this.router.navigate(['/boats']);

      }, error => {  this.snackBar.showSnackBar(error.error, "ok")})
    }
  }

  async signUpUser() {
    await this.router.navigate(['/signup'])
  }
}
