import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {UserService} from "../../services/user/user.service";
import {Router} from "@angular/router";
import {SnackbarService} from "../../services/snackbar/snackbar.service";
import {trigger} from "@angular/animations";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  signUpForm = new FormGroup({})
  public hasIncorrectPassword : boolean = false;
  constructor(private router : Router,  private userService : UserService, private snackBarSercice : SnackbarService) { }

  ngOnInit(): void {


    this.signUpForm = new FormGroup({
      username : new FormControl('', [Validators.required],),
      password : new FormControl('', [Validators.required, ],),
      secondPassword : new FormControl('', [Validators.required, ],)

    }, {validators :this.checkPasswords})
  }

  get username(){
    return this.signUpForm.get('username')
  }
  get password(){
    return this.signUpForm.get('password')
  }
  get secondPassword(){
    return this.signUpForm.get('secondPassword')
  }

  async signUp() {
    if (this.signUpForm.valid) {
      this.hasIncorrectPassword = true;
      const result = await this.userService.signUpUser(this.username?.value, this.password?.value);
      result.subscribe(async value => {
        this.snackBarSercice.showSnackBar(value, "ok");
        await this.router.navigate(['/login'])
      },)
    }
    else{
      if(this.signUpForm.errors!['notSame']){
        this.hasIncorrectPassword = true;
      }
    }

  }
  checkPasswords: ValidatorFn = (signUpForm: AbstractControl):  ValidationErrors | null => {
    let pass = this.password?.value
    let confirmPass = this.secondPassword?.value
    return pass === confirmPass ? null : { notSame: true }
  }


}
