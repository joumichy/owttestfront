import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { BoatFormComponent } from './boat/boat-form/boat-form.component';
import { BoatsComponent } from './boat/boats/boats.component';
import {RouterModule, Routes} from "@angular/router";
import { NotFoundComponent } from './components/not-found/not-found.component';
import {ReactiveFormsModule} from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {HttpClientModule} from "@angular/common/http";
import {MatButtonModule} from "@angular/material/button";
import {MatSortModule} from "@angular/material/sort";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import { MatDialogModule} from "@angular/material/dialog";

import {MatSnackBarModule} from '@angular/material/snack-bar';
import { SignUpComponent } from './auth/sign-up/sign-up.component';
import {BoatModule} from "./boat/boat.module";


const routes : Routes = [
  {path : "", redirectTo : "/login", pathMatch : "full",},
  {path : "login", component : LoginComponent},
  {path : "signup", component : SignUpComponent},

  {path : "boats", component : BoatsComponent},
  {path : '404', component : NotFoundComponent},
  {path : "**", component : NotFoundComponent},
]
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    SignUpComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {onSameUrlNavigation: "reload", useHash: true}),
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatSnackBarModule,
    BoatModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
