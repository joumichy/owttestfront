import {UserModel} from "./UserModel";

export interface BoatModel {

  "id": number,
  user? : UserModel
  "name": string,
  "size": number,
  "description": string,
}
