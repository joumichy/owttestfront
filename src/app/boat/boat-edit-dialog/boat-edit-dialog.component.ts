import {Component, Inject, OnInit} from '@angular/core';
import {BoatService} from "../../services/boat/boat.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {BoatModel} from "../../models/Boat";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {observable} from "rxjs";
import {SnackbarService} from "../../services/snackbar/snackbar.service";

@Component({
  selector: 'app-boat-edit-dialog',
  templateUrl: './boat-edit-dialog.component.html',
  styleUrls: ['./boat-edit-dialog.component.css']
})
export class BoatEditDialogComponent implements OnInit {

  public data : BoatModel | undefined;
  public boatForm : FormGroup = new FormGroup({})

  constructor(private boatService : BoatService,
              public dialogRef : MatDialogRef<BoatEditDialogComponent>,
              private snackBar : SnackbarService,
              @Inject(MAT_DIALOG_DATA) public boatModel : any){
  }


  ngOnInit(): void {
    this.data = this.boatModel.boatModel;
    this.boatForm = new FormGroup({
      name : new FormControl(this.data?.name,[Validators.required],),
      size : new FormControl(this.data?.size, [Validators.required],),
      description : new FormControl(this.data?.description,),
    })
  }

  get name(){
    return this.boatForm.get('name')
  }
  get size(){
    return this.boatForm.get('size')
  }
  get description(){
    return this.boatForm.get('description')
  }

  cancel() {
    this.dialogRef.close(false);
  }

  async setData() {


    if (this.boatForm.valid) {
      const result= await this.boatService.editBoat(this.data?.id, this.name?.value, this.size?.value, this.description?.value)
      await result.subscribe(value =>{
        this.snackBar.showSnackBar(value.message,"OK")}, error => {
        this.snackBar.showSnackBar(error.error,"OK")}
      )
      this.dialogRef.close(true)
    }

  }
}
