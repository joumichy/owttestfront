import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {BoatService} from "../../services/boat/boat.service";
import {SnackbarService} from "../../services/snackbar/snackbar.service";

@Component({
  selector: 'app-boat-form',
  templateUrl: './boat-form.component.html',
  styleUrls: ['./boat-form.component.css']
})
export class BoatFormComponent implements OnInit {


  boatForm = new FormGroup({
    name : new FormControl('', [Validators.required],),
    size : new FormControl('', [Validators.required, Validators.min(1)],),
    description : new FormControl('',),
  })

  constructor(private boatService : BoatService, private snackBar : SnackbarService) { }

  @Output() newItemEvent = new EventEmitter<boolean>();

  ngOnInit(): void {
  }


  get name(){
    return this.boatForm.get('name')
  }
  get size(){
    return this.boatForm.get('size')
  }
  get description(){
    return this.boatForm.get('description')
  }


  async createBoat() {

    if(this.boatForm.valid){
      const result = await this.boatService.createBoat(this.name?.value, this.size?.value, this.description?.value);
      await this.newItemEvent.emit(true);
      result.subscribe(async value => {
          this.snackBar.showSnackBar(value.message,"ok");
        },
        error => {
          this.snackBar.showSnackBar(error.error,"ok");
        })
    }
  }
}
