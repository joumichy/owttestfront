import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {BoatsComponent} from "./boats/boats.component";
import {BoatFormComponent} from "./boat-form/boat-form.component";
import {BoatEditDialogComponent} from "./boat-edit-dialog/boat-edit-dialog.component";
import {MatSortModule} from "@angular/material/sort";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {NavbarComponent} from "../components/navbar/navbar.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";



@NgModule({
  declarations: [
    BoatsComponent,
    BoatFormComponent,
    BoatEditDialogComponent,
    NavbarComponent,


  ],
  imports: [
    CommonModule,
    MatProgressBarModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,


  ]
})
export class BoatModule { }
