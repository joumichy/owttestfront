import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {BoatModel} from "../../models/Boat";
import {BoatService} from "../../services/boat/boat.service";
import {UserModel} from "../../models/UserModel";
import {UserService} from "../../services/user/user.service";
import {MatDialog} from "@angular/material/dialog";
import {BoatEditDialogComponent} from "../boat-edit-dialog/boat-edit-dialog.component";
import {SnackbarService} from "../../services/snackbar/snackbar.service";
import {Router} from "@angular/router";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-boats',
  templateUrl: './boats.component.html',
  styleUrls: ['./boats.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class BoatsComponent implements OnInit {

  dataSource : MatTableDataSource<BoatModel>;
  displayedColums : string[] = ["name","owner","size","expand","edit","delete"];
  currentUser : any;
  expandedElement: BoatModel | undefined;
  public isDataLoading : boolean = false;

   constructor(private boatService: BoatService,
               private userService: UserService,
               private snackBar : SnackbarService,
               private dialog : MatDialog,
               private router : Router) {
    this.dataSource = new MatTableDataSource<BoatModel>();
  }

  async ngOnInit(): Promise<void> {
    this.currentUser = await this.userService.getUserData();
    await this.getData(true);
  }

  hasProperty(username  : string){
      return this.currentUser.username == username;
  }

  async getData(withLoading : boolean) {
     if(withLoading) this.isDataLoading = true;
    const result = await this.boatService.getAllBoats();
    result.subscribe(value => {
      this.dataSource.data = value;
      this.isDataLoading = false;
    }, error => {
      if(error.status == 401){
        this.router.navigate(['/login'])
        this.isDataLoading = false;
      }
    })
  }

  async hasBoatCreated(boatCreated: boolean) {
    if (boatCreated) await this.getData(false);
  }

  editBoat(element: BoatModel) {
    const dialogRef = this.dialog.open(BoatEditDialogComponent,{
      width : '500px',
      data : {boatModel : element}

    })

    dialogRef.afterClosed().subscribe(result => {
      if(result) this.getData(false);
    })
  }

  async  deleteBoat(element : BoatModel) {
    //TODO :
   const result = await this.boatService.deleteBoat(element.id);
   result.subscribe(value => {
     console.log(value);
     this.snackBar.showSnackBar(value.message,"OK")

     this.getData(false)})
  }
}
