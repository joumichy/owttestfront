# Owttest


Auteur : John ALLOU

## Information

- Appliation FrontEnd Angular permettant l'utisation des methodes CRUD sur une liste de bateau via un utilisateur

- Vous devez disposer d'un compte ou en creer (VIA application) pour utiliser l'application.

- Le projet respecte toutes les contraintes imposées dans les spec.

- l'API est utilisable avec sa partie back: https://gitlab.com/joumichy/owttest

## Utilisation :

    - Pour installer les dépendencances : npm install
    - Pour lancer le projet en local sur le port 4200 : npm run start


